<?php

namespace Quatius\Promo\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Promo::index");
    }

    public function applyMembership(Request $request)
    {
        try {
            if (!auth()->guest()){
                $user = user();
                if (setVIPMember($user, $request->all(), 3)){
                    $order= app('OrderSession');
                    $order->setCustomer($user);
                    $order->save();
                    return response()->json(
                        [
                            'message'  => "Your membership has been updated",
                            'code'     => 204,
                            'view' => view("Promo::membership.profile")->render()
                        ],
                        201);
                }
                else{
                    return response()->json(
                        [
                            'message'  => "Failed to assign membership, please check your membership code",
                            'code'     => 400,
                        ],
                        400);
                }
            }
            
            return response()->json(
                [
                    'message'  => "Please login to apply Membership",
                    'code'     => 400,
                ],
                400);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
        return response()->json(
            [
                'message' => 'unknown error',
                'code'    => 400,
            ],
            400);
    }
    
}
