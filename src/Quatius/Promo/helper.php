<?php

if (!function_exists('setVIPMember'))
{
    function setVIPMember($user, $datas, $shopperId=0, $save=true){
        
        if (isset($datas['membership_code'])){
            $code = trim($datas['membership_code']);
            $type = getMemberType($datas['membership_code']);
            if ($type)
            {
				if ($shopperId == 0){
                    $shopperId = setting('promo.membership.shopper', 'default', 0);// 3 = VIP
                }
                
                $user->setParams('membership.types.'.$type, $code);
                $shopperId = setting('promo.membership.shopper', $type, $shopperId);

                $user->setParams('membership.shopper.'.$type, $shopperId);

                $shopperRepo = app('ShopperHandler');
                $shopper =  $shopperRepo->findWhere(['id'=>$shopperId])->first();
				
				if (setting('promo.membership.eval',$type,''))
				{
					try{
						eval(setting('promo.membership.eval',$type,''));
					}catch(\Exception $e){
					}
				}
				
                if($save){
                    if($shopper)
                        $shopperRepo->updateShoppers([$shopperId],$user);
                    
                    $user->save();
                }else{ // unsave;
                    foreach (app('ShopperHandler')->getUserShopperGroups($user) as $savedShopper){
                        $user->shoppers->add($savedShopper);
                    }
                    
                    if($shopper)
                        $user->shoppers->add($shopper);
                }
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('getMemberType'))
{
    function getMemberType($code){
        $code = trim($code);
        foreach (setting('promo.membership.codes') as $type => $value) {
            $listCode = explode(',',$value);
            if (array_search(strtoupper($code),$listCode) !== false){
                return $type;
            }
        }
        return "";
    }
}