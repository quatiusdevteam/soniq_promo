<?php

Route::group(['module' => 'Promo', 'middleware' => ['web', 'auth'], 'namespace' => 'Quatius\Promo\Controllers'], function() {
    Route::post('promo/membership/apply', 'PromoController@applyMembership');
});