<div class="row">
	<div class='col-md-12'>
	    {!! Form::textarea('components['.$index.'][data][model_list]')
        -> label("Model List:")
        -> id('model_list'.$index)
        -> value($component->getData()->model_list)
        !!}
    </div>
</div>