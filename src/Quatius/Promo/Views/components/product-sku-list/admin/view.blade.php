<div class="well text-center pull-left">
  @php
  $order = app('OrderSession');
  $repo = app('ProductHandler');
  $models = explode(',',str_replace(" ","",$component->getData()->model_list));
  $prods =  $repo->makeModel()->query()->whereIn('sku',$models)->published()->ofShoppers($order->getShopperGroupIds())->get();
  $modelOrder = array_flip($models);
  
  $products = $repo->preloadViewingProducts($prods, $order->getShopperGroupIds()[0]);
  
  $products = $products->sortBy(function($item) use($modelOrder){
      return $modelOrder[$item->sku];
  })->values();

  $tilePath = view('Promo::components.product-sku-list.admin.tile')->getPath();
  $tileBlade = file_get_contents($tilePath); // optimus for loading

  @endphp
  <h3>{{$component->name}}</h3>
  @foreach ($products as $product)
  <div class="img-thumbnail" style="width: 120px; float:left; margin: 2px">
    {!! renderBlade($tileBlade, ['product' => $product]) !!}
	</div>
  @endforeach

</div>