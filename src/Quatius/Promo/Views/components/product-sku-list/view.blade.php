@php
$order = app('OrderSession');
$repo = app('ProductHandler');
$models = explode(',',str_replace(" ","",$component->getData()->model_list));
$prods = $repo->getBy('sku', $models, $order->getShopperGroupIds());
$modelOrder = array_flip($models);

$products = $repo->preloadViewingProducts($prods, $order->getShopperGroupIds()[0]);

$products = $products->sortBy(function($item) use($modelOrder){
    return $modelOrder[$item->sku];
})->values();

@endphp

@includeStyle('css/shop/shop-product.css')
@includeScript('js/shop/shop-cart.js')
<div class="product-list" id="prodList">
@include('Shop::product.partials.list', compact(['products','order']))
</div>