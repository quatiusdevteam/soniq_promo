<div class="membership-area">
    <div class="clearfix"><br><br></div>
    <h3>Member Benefits</h3>
    
    <div class="col-xs-12">
    <img src="{{media_url(setting('promo.membership.badges','Register',''), 120, 120)}}" style="float:left">
    
    <?php
        $members = user()->getParams("membership.types",[]);
        foreach ($members  as $type=>$code){?>

        <a href="{{setting('promo.membership.badges_link',$type,'#')}}" style="color: inherit;">
            <img src="{{media_url(setting('promo.membership.badges',$type,''), 120, 120)}} " style="float:left">
        </a>
    <?php } ?>
    </div>
    <div class="col-xs-12">
    <div class="clearfix"><br><br></div>
    <div class="input-group input-group-lg">
            <span class="input-group-addon">Membership:</span>
            <input type="text" class="form-control" id="membership_code" placeholder="Code" name="membership_code" autocomplete="off" value="">
            <span class="input-group-btn">
                <button class="btn btn-primary"  data-loading-text="Checking..."  onclick="updateMembership(this)" type="button">Apply</button>
            </span>
    </div>
    </div>
</div>
@script
<script>
    function updateMembership(itm){
        if (itm.value.trim() != "") return;
        $(itm).button('loading');
        $.post("{{url('promo/membership/apply')}}", $(itm).parents('.membership-area').find('input').serialize(), 
            function(response){
                $(itm).parents('.membership-area').replaceWith(response.view);
                $(itm).button('reset');
        }, 'json').fail(function(){
            $(itm).button('reset');
        });
    }
</script>
@endscript