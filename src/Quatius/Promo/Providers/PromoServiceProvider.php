<?php

namespace Quatius\Promo\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use Theme;
use Illuminate\Http\Request;
/**
 * @author seyla.keth
 *
 */
class PromoServiceProvider extends ServiceProvider
{
	
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		config()->set('quatius.component.types.product-sku-list',[
            'name'=>'Product List(skus)',
            'type'=>'product-sku-list',
            'view'=>'Promo::components.product-sku-list',
            'position'=>'content-footer',
			'detachable_by'=>'component.allow-detach',
			'editable_by'=>'admin',
            'selectable'=>true,
            'default'=>[
                'compiler'=>'blade',
                'model_list'=>'',
            ],
            
        ]);
	}
	
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		Event::listen('auth.login', function($user){
			$order= app('OrderSession');
			if ($order->hasParams('user_membership_code')){
				setVIPMember($user, ['membership_code'=>$order->getParams('user_membership_code', '')]);
				$order->unsetParams('user_membership_code');
				$order->save();
			}
		});
		//$events->listen('auth.logout', 'Quatius\Shop\Events\ShopListeners@onUserLogout');

		Event::listen("shop.cart-update", function($order, $request){
			if($request->has('coupon_code')){
				$code = $request->get('coupon_code', '');
				$type = getMemberType($code);

				if ($type){
					// remove for request avoid coupon processing
					$request->request->remove('coupon_code');
					$user = user();
					$order->setParams('user_membership_code', $code);
					

					if (auth()->guest()){
						setVIPMember($user, ['membership_code'=>$code], 0, false);
						$memberParams = json_decode(setting("promo.membership.params", $type, "{}"), true);

						$order->setAllowGuest(array_get($memberParams,"guest_checkout", false));
						
						$order->setCustomer($user, $user->shoppers);
						flash("Voucher code apply, please proceed to checkout");
					}else{
						setVIPMember($user, ['membership_code'=>$code]);
						$order->setCustomer($user);

						flash("Voucher code apply to your account, please proceed to checkout");
					}
					
				}
			}
			
		});

	    Event::listen('auth.register', function($user, $post_data) {
			setVIPMember($user, $post_data);
			
			$order= app('OrderSession');
			$order->setCustomer($user);
			$order->save();
			
		});

		Event::listen("shop.cart-setting-customer", function($order, $user){
			$types = array_keys($user->getParams('membership.types',[]));
			foreach($types as $type){
				$setEval = setting("promo.membership.on_new_order", $type, "");
				if ($setEval)
				{
					try{
						eval($setEval);
					}catch(\Exception $e){
					}
				}
			}
		});

		Event::listen("shop.cart-calculating-discount", function($order){
			if(($order->hasParams('discounts.coupon'))) {
				$order->unsetParams('discounts.membership');
			}else{
				
				$user = $order->getCustomer();
				$types = array_keys($user->getParams('membership.types',[]));
				$membershipType = end($types);
				$membershipType = $membershipType?:"NORMAL";
				$memberParams = json_decode(setting("promo.membership.params", $membershipType, "{}"), true);

				if ($memberParams){
					//promo.membership.params type:
					//{"discount":{"name":"Partner Discount","value":15,"min_order":50}}
					//{"discount":{"name":"Partner Discount","value":0,"percentage":0.05}}
					if(isset($memberParams["discount"]["percentage"])){
						$memberParams["discount"]["value"] = number_format($order->subtotal * floatval($memberParams["discount"]["percentage"]),2);
					}

					if(isset($memberParams["discount"]["min_order"]) && $order->subtotal >= $memberParams["discount"]["min_order"]){
						$order->setParams('discounts.membership', $memberParams["discount"]);
					}
					else if(isset($memberParams["discount"])){
						$order->setParams('discounts.membership', $memberParams["discount"]);
					}
					else{
						$order->unsetParams('discounts.membership');
					}
				}
			}
		});

		Event::listen('router.matched', function($route, $request) {
			
			if($route->uri()) { // route name
				if ($route->uri() == 'register'){
					Theme::append('register_extra',view("Promo::membership.register"));
				}
				
				if ($route->uri() == 'account/profile'){
					Theme::append('profile_extra',view("Promo::membership.profile"));
				}
				// do something
			}
			
			if ($route->uri() == 'shop/cart/address/{session_name?}'){
				Theme::append('register_extra',view("Promo::membership.cart_signup"));
			}
			//  Other useful things to check
			// Get current action name (someController@aMethod)
			// $route->getActionName();
			// // Get current url
			// $route->uri();
			// // Get HTTP methods it responds to
			// $route->getMethods();
			// // Get only username parameter
			// $route->parameter('username');
			// // Get all parameters
			// $route->parameters();
			// app('log')->info('Route - '.$route->getName().'('.$route->uri().'): ' );
		
			
			// // Request object
			// //$request->ajax() // is an ajax request
			// //$request->is('/') // is home page
			// // Check a parameter (username)
			// $username = $route->getParameter('username');
			// if($username && $username == 'heera') {
			//     //dd("Username is $username");
			// }
		});
	}
	
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['quatius'];
	}
}